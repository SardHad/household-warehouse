package org.example;

import org.example.controller.ItemController;
import org.example.entity.ItemEntity;
import org.example.service.Service;

import java.util.List;
import java.util.Scanner;

public class Main {
    private static Long id = 1L;

    public static void main(String[] args) {
        ItemController itemController = new ItemController();
        boolean runnig = true;
        System.out.println("Project");
        while (runnig) {
            try {


                Scanner scanner = new Scanner(System.in);

                System.out.println("""
                        1. ADD ITEM
                        2. EDIT ITEM
                        3. GET ALL ITEM
                        4. REMOVE ITEM
                        5. SEARCH
                        0. EXIT
                        """);
                int command = scanner.nextInt();

                if (command == 1) {
                    scanner = new Scanner(System.in);

                    System.out.println("Enter name item");
                    String name = scanner.nextLine();

                    System.out.println("Enter price item only number");
                    scanner = new Scanner(System.in);
                    while (!scanner.hasNextDouble()){
                        System.out.println("Please write in number form");
                        scanner.next();
                    }
                    Double price = scanner.nextDouble();


                    scanner = new Scanner(System.in);

                    System.out.println("Enter color item");
                    String color = scanner.nextLine();

                    System.out.println("Enter registrationDate item");
                    String registrationDate = scanner.nextLine();

                    System.out.println("Enter brand item");
                    String brand = scanner.nextLine();

                    System.out.println("Enter category item");
                    String category = scanner.nextLine();

                    itemController.add(new ItemEntity(
                            id++,
                            name,
                            price,
                            color,
                            registrationDate,
                            brand,
                            category
                    ));
                    System.out.println("Successfully added");
                }

                if (command == 2) {
                    List<ItemEntity> brands = itemController.getAll();
                    brands.forEach(System.out::println);
                    System.out.println("Enter id editing item");

                    scanner = new Scanner(System.in);
                    Long id = scanner.nextLong();

                    scanner = new Scanner(System.in);

                    System.out.println("Enter new name item");
                    String name = scanner.nextLine();

                    System.out.println("Enter new price item");
                    Double price = scanner.nextDouble();

                    scanner = new Scanner(System.in);

                    System.out.println("Enter new color item");
                    String color = scanner.nextLine();

                    System.out.println("Enter new registrationDate item");
                    String registrationDate = scanner.nextLine();

                    System.out.println("Enter new brand item");
                    String brand = scanner.nextLine();

                    System.out.println("Enter new category item");
                    String category = scanner.nextLine();

                    itemController.edit(
                            id,
                            new ItemEntity(
                                    id,
                                    name,
                                    price,
                                    color,
                                    registrationDate,
                                    brand,
                                    category
                            ));
                    System.out.println("Successfully edited");
                }

                if (command == 3) {
                    List<ItemEntity> brands = itemController.getAll();
                    brands.forEach(Service::getFormatEntity);
                }
                if (command == 4) {
                    scanner = new Scanner(System.in);
                    System.out.println("Please enter you deleting item Id");
                    Long id = scanner.nextLong();

                    itemController.remove(id);
                    System.out.println("Successfully deleted");
                }
                if (command == 5) {
                    List<String> brands = itemController.getBrands();
                    System.out.println("Example brands");
                    brands.forEach(System.out::println);

                    List<String> categories = itemController.getCategories();
                    System.out.println("Example Categories");
                    categories.forEach(System.out::println);

                    System.out.println("Enter searching text");

                    scanner = new Scanner(System.in);
                    String text = scanner.nextLine();

                    List<ItemEntity> search = itemController.search(text);
                    search.forEach(System.out::println);


                }
                if (command == 0) {
                    runnig = false;
                }
            } catch (Exception e) {
                System.err.println("ERROR");
            }
        }


    }
}


