package org.example.controller;

import org.example.entity.ItemEntity;
import org.example.service.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class ItemController {

    private Service service;

    public ItemController() {
        service = new Service();
    }

    public List<ItemEntity> add(ItemEntity item) {
       return service.add(item);
    }

    public boolean edit(Long id, ItemEntity editedItem) {
       return service.edit(id,editedItem);
    }

    public ItemEntity getById(Long id) {
       return service.getById(id);
    }


    public List<ItemEntity> getAll() {
       return service.getAll();
    }

    public List<ItemEntity> search(String searchingText) {
       return service.search(searchingText);
    }
    public List<String> getBrands() {
      return service.getBrands();
    }
    public List<String> getCategories() {
      return service.getCategories();
    }

    public boolean remove(Long id) {
       return service.remove(id);
    }


}
